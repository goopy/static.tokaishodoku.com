var gulp = require('gulp'),
    browser = require('browser-sync'),
    $ = require('gulp-load-plugins')(),
    paths = {
      src: '../../asset/src',
      publish: '../../',
      dist: '../../asset',
      docs: 'docs'
    };

gulp.task('server',function(){
  browser({
    notify:false,
    // proxy:'local.accounts.hybs.jp'
    server:{
      baseDir: paths.publish
    }
  });
  gulp.watch(paths.src+'/less/**/*.less',['less']);
  gulp.watch([
    paths.publish+'/**/*.html',
    paths.publish+'/**/*.php',
    paths.publish+'/**/*.xml',
    paths.dist+'/js/**/*.js',
    paths.dist+'/html/**/*.html'
  ]).on('change',browser.reload);
});

gulp.task('less',function(){
  gulp.src([
      paths.src+'/less/**/*.less',
      '!'+paths.src+'/less/**/_*.less',
      '!'+paths.src+'/less/turret/**/*.less',
      '!'+paths.src+'/less/elements/**/*.less',
      '!'+paths.src+'/less/components/**/*.less',
      '!'+paths.src+'/less/fonts/**/*.less'
    ],{ base: paths.src+'/less' })
    .pipe($.plumber({errorHandler: $.notify.onError('Compile Error:<%= error.message %>')}))
    .pipe($.sourcemaps.init())
    .pipe($.less())
    .pipe($.cssnano())
    .pipe($.rename({suffix:'.min'}))
    .pipe($.sourcemaps.write('./maps/'))
    .pipe(gulp.dest(paths.dist+'/css'))
    .pipe(browser.reload({
      stream: true,
      once: true
    }));
});

gulp.task('svgSprite', function () {
  gulp.src(paths.src+'/svg/**/*.svg')
    .pipe($.svgSprite({
      mode: {
        symbol:{
          dest: './',
          inline: true
        }
      }
    }))
    .pipe(gulp.dest(paths.dist));
});


gulp.task('default',['server']);