(function(w,d,$){
  'use strict';

  $(function(){
    // 【汎用】青色ブロックにコンテンツ名のクラス名を付与(PC/SP)
    (function(_){
      $(_.target).each(function(){
        var $target = (is_mobile())? $(this):$(this).parent('.block');
        $target.addClass('p-'+_.cont(this)[1]+'Block');
      });
    }({
      cont : function(obj){
        return $(obj).find( (is_mobile())?'h3 > a':'a.wrapLink' ).attr('href').match(/^\/([^\/]+)\//)
      },
      target : (is_mobile())? '.ui-mobile .homeBlock':'#main .block [class*="layout_b_pattern"]'
    }));


    // ニュースブロック整形
    (function(_){
      _.$articles.coverThumb().each(function(){
        var $this = $(this),
            $catetitle = $('.catetitle',this),
            $date = $('.date',this),
            $new = $('.new',this),
            $title = $('h5',this).addClass('title'),
            $catch = $('.catch',this),
            $body = $('p:last-child',this).addClass('body');
        $date.text( $date.text().replace('日','').split(/\/|\.|年|月/).join(_.dateSep) );
        $catetitle.html( $('a:last-child',$catetitle) );
        
        $this
          .append( $date )
          .append( $catetitle )
          .append( $title.append($new) )
          .append( $catch )
          .append( $body );
        // $date.insertAfter( $catetitle );
      });
      // $('.element_block > .contents',_.$block).getCatList({
      //   content: 'news',
      //   blockClass: 'newsCat',
      //   addAll: true,
      //   linkClass:'button'
      // });
      if($.UA.is_mobile){
        var $title = _.$block.find('h3.ui-bar'),
            titleHtml = '<span>NEWS &amp; TOPICS</span><small>お知らせ</small>';
        $title.html(titleHtml).removeClass('ui-bar').addClass('p-newsBlock__title');
        $title.find('a').remove();
      }

      var $blockTitle_link = $('.element_block > a.wrapLink:first-child',_.$block);
      $blockTitle_link.before( $blockTitle_link.find('h3').addClass('p-newsBlock__title')  );
      $blockTitle_link.remove();

      var $button = $(_.button.tag).attr(_.button.attr),
          $buttonWrapper = $(_.buttonWrapper.tag).attr(_.buttonWrapper.attr).append($button);
      _.$block.append($buttonWrapper);
    }({
      $block:     $('.p-newsBlock'),
      $articles:    $('.p-newsBlock .artloop','#main'),
      buttonWrapper:  { tag:'<div />', attr:{ class:'button-wrap text-center'}},
      button:{
        tag: '<a>MORE</a>',
        attr:{
          href:  '/news/',
          class: 'button button-center button-normal button-icon-right'
        }
      },
      'dateSep':'.'
    }));


  });

}(window,document,jQuery));