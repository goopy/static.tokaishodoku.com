(function(w, d, $){
  'use strict';
  
  $.require({
    disableCache:false,
    '*':[
      '(js)https://maps.googleapis.com/maps/api/js?key=AIzaSyDBB_DXqmoG89lUoaokvVH58OZbGj9bq0s',
      '/asset/js/vendor/jquery.easing.1.3.js'
    ],
    pc:{
      '/':[
        '/asset/js/vendor/slick.min.js',
        '/asset/css/vendor/slick/slick.min.css',
        '/asset/css/vendor/slick/slick-theme.min.css'
      ]
    },
    sp:{
      '*':[
        '/asset/js/vendor/modernizr.js'
      ]
    }
  });
  $.require({
    disableCache:false,
    '*':{
      '/':[
        '/asset/css/top.min.css',
        '/asset/js/top.js'
      ],
      '/business':[
        '/asset/css/business.min.css'
      ],
      '/about':[
        '/asset/css/about.min.css'
      ],
      '/recruit':[
        '/asset/css/recruit.min.css'
      ]
    }
  });

  $(function(){

    // SP用フッターナビ読み込み
    (function(){
      if(!is_content('/') && !is_content('inquiry')){
        $('.pageBottomLink','#footer').spFooterNav({
          data:{
            tel: '000-000-0000',
            title:{
              form: 'お問い合わせフォーム'
            }
          }
        });
      }
    }());

    //header.html, footer.html読み込み
    (function(_){
      $.each(_,function(i,__){
        var mes = 'loadhtml error: ';
        $.get(__.loadhtml)
          .done(function(html){
            if(__.$target[__.method](html).length != 0){ __.callback(this,__) }
            else{
              mes+='Not found DOM $(\'%s\').\nCouldn\'t load \'%s\'';
              console.error(mes,__.$target.selector,this.url);
            }
          }).fail(function(data){ //ajaxの通信に失敗した場合
            mes+='Ajax failed \'%s\'\n%s (%s)';console.error(mes,this.url,data.statusText,data.status);
            return false;
          });
      });
    }([
        {
          loadhtml: '/asset/html/footer.html',
          $target: $('footer #footerNav'),
          method:'replaceWith',
          callback: function(r,_){
            console.info('%c%s%c is loaded','font-weight: bold',r.url,'');
          }
        },
        {
          loadhtml: '/asset/html/header.html',
          $target: $('header'),
          method:'append',
          callback: function(r,_){
            // gmenu current
            $('header .header__gmenu__list').currentMenu({mode:1,default:999});

            console.info('%c%s%c is loaded','font-weight: bold',r.url,'');
          }
        }
    ]));

    $.bodyClass();

    //svgスプライト読み込み
    $.loadSVG();

    // htmlデータ読み込み
    $('[data-load-html]').loadHtml();

    $('[data-plugin=openwindow]').on('click',function(e){
      e.preventDefault();
      var $this = $(this),
          data = $this.data(),
          size = 'width='+(data.width||600)+', height='+(data.height||500)+',';
      w.open(this.href,(data.title||'share this site'),size+"menubar=no, toolbar=no, scrollbars=yes");
    });


    // $.ajaxSetup({ cache: false });
    // $.getScript('//connect.facebook.net/ja_JP/sdk.js', function(){
    //   FB.init({
    //     appId: '759600244139396',
    //     xfbml: true,
    //     version: 'v2.8'
    //   });
    // });

    // スマホフッターカスタム
    (function(_){ if(!$.UA.is_mobile && _.$footer.length == 0) return;
      _.$footer.parent().insertAfter( _.$needle );
    }({
      $footer: $('.detailFooter .pageingBtnBlock'),
      $needle: ( $('.listContents').length )? $('.listContents') : $('.element_detail').parent()
    }));

    // ページングテキスト
    $.pagingText({
      prev:'PREV',
      next:'NEXT',
      list:'LIST'
    });

    // プライバシー・会社概要・サイトマップのコンテンツタイトル
    (function(cnt,path){
      var page = path.match(/^\/([^\/]+)\.php$/i);
      if(!page || !cnt[page[1]]) return false;
      var $title = $('<h2>').text(cnt[page[1]]),
          $container = $('<section class="mainImg"><div class="mainImgInner"></div></section>');
      $title.appendTo($container.find('.mainImgInner'));
      $container.insertAfter('#gmenu');
      $('.moduletitle').remove();
      var bodyClass = $('body').attr('class').replace(/[?|&]ipsw=\d/,'');
      $('body').attr('class',bodyClass);
    }({
      'sitemap':'サイトマップ',
      'privacy':'プライバシーポリシー',
      'profile':'会社概要'
    },location.pathname));

  });

  $(w).on('load',function(){
    console.info('this is onload function');

    // 電話番号リンク
    $('a[data-tel]').each(function(){
      if($.UA.device == 'iphone' || $.UA.device == 'androidmobile'){
        $(this).attr('href','tel:'+$(this).data('tel'));
      }
    });

    // 一覧と詳細表示イベント
    (function(cls){
      $.each(cls,function(key,val){
        if( $('body').hasClass(val) ) $(w).trigger(key+'_disp');
      });
    }({'detail':'is--detail','list':'is--list'}));

    setTimeout(function(){

      // googlemap読み込み
      (function(getstyle){
        if( $('[data-plugin="googlemap"]').length > 0 ){
          var $map = $('[data-plugin="googlemap"]');
          getstyle.done(function(mapstyle){
            var styleObj = { name:'mapstyle', style:mapstyle};
            $map.googlemap({styleObj:styleObj});
          });
        }
      }( $.get('/asset/json/mapstyle.json') ));

      // table.responsiveを外包するwrapperを作る(readyfunctionに移す)
      if( is_mobile() ){
        (function(_){
          _.$table.each(function(){
            $(this).wrapAll($('<div />').addClass(_.wrapperClass));
          });
        }({
          $table:$('table.responsive'),
          wrapperClass:'tableResponsiveWrappr'
        }));
      }

      // タップするとマップが操作できるオーバーレイ表示(スマホのみ)(ready functionに移す)
      if( is_mobile() ){
        (function(_){
          _.$map.each(function(){
            var $this = $(this),
                overlayStyle = $.extend(_.overlayStyle,{
                  top: $this.outerHeight(true)*-1,
                  width:$this.width(),
                  height:$this.height(),
                  marginBottom:$this.height()*-1
                }),
                $overlay = _.$overlay.clone();
            $this.after( $overlay.css(overlayStyle) );
            $overlay.on(_.method,function(e){
              $overlay.remove();
            });
          });
        }({
          $map:$('[data-plugin=googlemap]'),
          method:'click',
          overlayStyle:{
            'position':'relative',
            'top':0,'left':0,'width':'100%','height':'100%',
            'z-index':10,
            'background-color':'rgba(255,255,255,.08)'
          },
          $overlay:$('<div class="overlay"/>')
        }));
      }

      // 記事内に[data-layout=elastic]があれば、window100%にする(readyfunctionに移す)
      $('#container').has('[data-layout=elastic]').addClass('elastic');

      //headerImage(ready functionに移す)
      (function(_){
        _.$target.each(function(){
          var $this = $(this),
              src = $this.data('image-src');
          $this.css('background-image','url('+src+')');
        });
      }( { $target: $('.contentHeader[data-image-src]') } ));

      //アンカーリンクスクロール設定
      $(d).on('click','a[href^="#"],a[href*="#"]',function(e){
        var path = $(this).attr('href').match(/^([^#]+)(\#.*)?/)||[];
        if( path[1] === location.pathname ){
          e.preventDefault();
          $(this).attr('href',path[2]);
        }
        var speed =   500,
            href =    $(this).attr('href'),
            $target = $(href == '#pagetop' || href == '#top' ? 'html' : href ),
            pos =     $target.offset().top;
            if($.UA.is_mobile){
              var headerHeight = $('.ui-header-fixed').height();
              pos = (pos - headerHeight);
            }
        $('body,html').animate({scrollTop:pos},speed,'easeInOutExpo');
        if(href!='#pagetop' && href!='#top')
          window.history.pushState(null, null, this.hash);
        return false;
      });

      // ハッシュURLでスクロール
      (function(_){
        if(_.hash != ''){
          var pos = $(_.hash).offset().top;
          if($.UA.is_mobile){
            var headerHeight = $('.ui-header-fixed').height();
            pos = (pos - headerHeight);
          }
          _.$target.animate({'scrollTop':pos},200,'easeInOutExpo');
        }
      }({
        hash:location.hash,
        $target:$('body,html')
      }));
      //jqueryMobileのアンカーリンク対策
      (function(){
        if(is_mobile()){
          $('a[href*="#"]').attr('data-ajax','false');
        }
        if(is_mobile() && location.hash){
          $(document).bind('pageshow',function(e){
            (function(w,scrolltop,headerHeight){
              $(document).bind("silentscroll",function(e,data) {
                $(this).unbind(e);
                $(w).scrollTop(scrolltop - headerHeight);
              });
            }('body,html', $(location.hash).offset().top , $('.ui-header-fixed').height() ));
          });
        }
      }());

      // ページトップリンクをスクロール量で表示する
      $.scrollspy({
        trigger:200,
        onBefore: function(pos){
          $('a.go-to-top').addClass('hidden');
        },
        onAfter: function(pos){
          $('a.go-to-top').removeClass('hidden');
        }
      });

    },100);
  });

}(window, document, jQuery));