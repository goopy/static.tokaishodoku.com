(function(templatePath){
  write_file(templatePath+'asset/js/fontchange.js');
  write_file(templatePath+'asset/js/modernizr.js');
  write_file(templatePath+'asset/js/socialbutton.js');
  write_file(templatePath+'asset/js/jquery.easing.1.3.js');
  write_file(templatePath+'asset/js/jquery.magnific-popup.min.js');
  write_file(templatePath+'asset/css/magnific-popup.css');
})(hsRoot+'_site_template/layout/100_01_white-gray/');

(function(w,d,t){
  var b=d.getElementsByTagName(t)[0];b.style='opacity:0';
}(window,document,'html'));

// $(function(){
//  var $root = $('body');
//  $root.css({'opacity':0});
// });

$(document).ready(function(){
  var $root = $('body');
  //lightbox設定
  (function($e){
    // カートのズームはズームしない
    $('.detail_photobox a.zoom').attr('rel','lightbox[zoom]');

    // rel=lightbox[]の値が同じだったらグループ化
    $.each( $.unique( $e.map(function(i,e){return $(e).attr('rel')}) ),function(i,v){
      $('a[rel*="'+v+'"]').magnificPopup({
        type: 'image',gallery:{ enabled:true},removalDelay:200,mainClass: 'mfp-fade',
        zoom:{ enabled: (v.indexOf('[zoom]') != -1)? false : true}
      });
    });
  }( $('a[rel*="lightbox"]') ));
  
  //カート詳細スライダー設定
  if($root.hasClass('is--module-cart') && $root.hasClass('is--detail')){
    $('.detail_photobox .thumbnails a').each(function(index){
      $(this).attr('data-slide-index',index);
    });
    var slider = $('.slider','.detail_photobox').bxSlider({
      'pagerCustom': '.detail_photobox .thumbnails',
      'nextSelector': '.detail_photobox .controls .next',
      'prevSelector': '.detail_photobox .controls .prev',
      'nextText':'',
      'prevText':''
    });
  }
  
  //ページスクロール設定
  $('a[href^=#]').on('click',function(){
    var speed =   500,
        href =    $(this).attr('href'),
        $target = $(href == '#pagetop' || href == '#top' ? 'html' : href ),
        pos =     $target.offset().top;
    $('body,html').animate({scrollTop:pos},speed,'easeInOutExpo');
    return false;
  });
  
  //ブログコメント欄表示スクリプト
  if($root.hasClass('is--module-blog') && $root.hasClass('is--detail')){
    $('#commentForm .commentFormTitle').click(function(){
      $(this).next('form').slideToggle('fast');
    });
  }
  
  //フォームSSL欄表示スクリプト
  if($root.hasClass('is--module-form') && $root.hasClass('is--detail')){
    $('#ExplainSSL').click(function(){
      $(this).toggleClass('open');
    });
  }
  
  //会社概要テーブル編集
  if($root.hasClass('is--profile')){
    var $table = $('.module_profile_table','#main');
    $table.find('tr').each(function(){
      var $self = $(this),
          $th = $self.children('th'),
          $td = $self.children('td');
      if($th.html() == '' || $th.html() == '&nbsp;'){
        $td.attr('colspan','2').css('padding','10px 0');
        $th.remove();
      }
    });
  }
  
  /************************************
   *  ブロック全体クリック
   ************************************/
  function wraplink(obj,href,addClass){
      addClass = addClass ? ' '+addClass:'';
    $(obj).wrapAll('<a href="'+href+'" class="wrapLink'+addClass+'"></a>');
  }
  //ブロックタイトル
  $('.element .extitle','body.is--home').each(function(){
    var href = $(this).find('.listicon a').attr('href');
    if(href) wraplink(this,href);
  });
  //記事ブロック：メインカラム
  $('.block .artloop,.element_list .artloop','#main').each(function(){
    var href = $(this).find('h5 a').attr('href'),
        addClass;
    if($(this).hasClass('list3')) addClass = "block-2column";
    if(href) wraplink(this,href,addClass);
  });
  //記事ブロック：サイドカラム
  $('.block .contents .contents').each(function(){
    var href = $(this).find('h5 a').attr('href');
    if(href) wraplink(this,href);
  });
  
  //一列画像なしブロックのタイトルのみ判別
  $('.block .artloop h5,.block .contents .contents h5').each(function(){
    var $self = $(this);
    if(!$self.siblings('p').length){
      $self.addClass('titleOnly');
      $self.parents('.wrapLink').addClass('titleOnly');
    }
  });
  
  ////100テンプレート
  if (window.PIE) {
    $('button').each(function() {
      PIE.attach(this);
    });$('.button').each(function() {
      PIE.attach(this);
    });
  }

  // ブロック・一覧のサムネイル画像を元画像にする
  (function($blocks){
    $.each($blocks,function(i,$article){
      $article.coverThumb();
    });
  }([
    $('#main > .block .artloop'),
    $('#main > .element_list .artloop')
  ]));


});
//ページ読み込み後実行
$(window).load(function(){
  var $root = $('html'),
      $body = $('body');
  //ページ遷移フェードイン／アウト
  $root.animate({ opacity: 1 }, 500,function(){$(this).css({opacity:''})});
  //グローバルメニュー・フッターメニューが960pxを超えたらクラスを付与する
  var $gmenu = $('nav#gmenu ul');
  var $fmenu = $('nav#footerNav ul');
  var windowWidth = $(window).width();
  if($gmenu.width() > windowWidth ) $gmenu.addClass('overwidth');
  if($fmenu.width() > windowWidth ) $fmenu.addClass('overwidth');
  //カテゴリのみ表示ブロックの高さ揃え
  // if( $('.layout_b_pattern6','#main').length ){
  //  var $category_only_block = $('.layout_b_pattern6','#main');
  //  $category_only_block.each(function(){
  //    var $catelist = $(this).find('.block_title a');
  //    adaptHeight($catelist,4);
  //  });
  // }
  //フォーム一覧：ボタンの高さ揃え
  if( $body.hasClass('is--module-form') && $body.not('.is--detail') ){
    var $buttons = $('.entry_formbanner .element a');
    adaptHeight($buttons,3);
  }
  function adaptHeight($list,column){
    var maxHeight = 0;
    $list.each(function(index){
      if(index % column ==0) maxHeight = 0;
      var $self = $(this);
      maxHeight = ($self.height() > maxHeight) ? $self.height() : maxHeight;
      if($list.length == index+1 && $list.length > column){
        column = (index+1) % column;
      }else if($list.length == index+1){
        column = $list.length;
      }
      if(index % column ==column -1 && index > 0){
        for(var i=0;i<column;i++){
          $($list[index-i]).height(maxHeight);
        }
      }
    });
  }
  
});

(function(w,d,$){
  'use strict';

  //coverThumb
  $.fn.coverThumb = function(option){
    var opt = $.extend(true,{
      thumbimg: '.thum_img img',
      thumbCss:{
        width:'100%',
        height:'100%',
        opacity:'0'
      }
    },option);
    return this.each(function(){
      var $this = $(this),
          thumbSrc = $this.find(opt.thumbimg).attr('src');
      if(thumbSrc==undefined || !thumbSrc) return;
      var rawImagePath = thumbSrc.replace(/thumb-([^\/]+)(\.jpe?g|gif|png)$/i,'$1$2');
      $this.find('.thum_img').addClass('cover-thumb').css({
        'background': 'url('+rawImagePath+') no-repeat center',
        'background-size':'cover'
      }).find('img').attr({
        src: rawImagePath
      }).css(opt.thumbCss);
    });
  }

}(window,document,jQuery));