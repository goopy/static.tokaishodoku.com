$(function(){
	var $root = $('body'),
			thisUrl = location.href.replace('main.php',''),
			$urls = {
				'fb':'https://www.facebook.com/sharer/sharer.php?u=',
				'tw':'http://twitter.com/share?url=',
				'gp':'https://plus.google.com/share?url=',
				'hb':'http://b.hatena.ne.jp/add?mode=confirm&url='
			},
			$container = $('<section class="socialButton"><ul></ul></section>');
	if( $root.hasClass('is--sns') ){
		$.each($urls,function(key,val){
			if( $root.hasClass('is--sns-'+key) ){
				if(_countsocial[key] >= 1000 && _countsocial[key] < 1000000){
					_countsocial[key] = Math.floor(_countsocial[key]/100)/10+'K';
				}
				if(_countsocial[key] >= 1000000 && _countsocial[key] < 1000000000){
					_countsocial[key] = Math.floor(_countsocial[key]/100000)/10+'M';
				}
				if(_countsocial[key] >= 1000000000 && _countsocial[key] < 1000000000000){
					_countsocial[key] = Math.floor(_countsocial[key]/100000000)/10+'G';
				}
				var $item = $('<a>').attr({'href':val+encodeURIComponent(thisUrl),'class':'sns-'+key}).text(_countsocial[key]).wrapInner('<span>').prepend('<i class="icon-'+key+'"></i>');
				$item.bind('click',function(){openWindow(this.href,key+' window');return false;});
				var $list = $('<li>').append($item);
				$list.appendTo($container.find('ul'));
			}
		});
	}
	if( $root.hasClass('is--sns') ){
		if( $root.hasClass('is--home') || $root.is('.is--profile,.is--sitemap,.is--privacy,.is--search') ){
			$container.appendTo('#side','#container');
		}
		if( $root.hasClass('is--detail') ){
			if( $root.hasClass('is--module-blog') ){
				$container.prependTo('div#comment_main','#container #main');
			}
			if( $root.hasClass('is--module-form') ){
				$('form[name="form1"]','#container #main').after($container);
			}else{
				$container.prependTo('div.entry+div.detailfooter','#container #main');
			}
		}
	}
	function openWindow(url,name){
		window.open(url, name, 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes');
	}	
});