$(document).ready(function(){
	var $root = $('html'),
			$fontChangeBtn = $('button[data-font-size]','section.fontChange'),
			fontSizeObj = {
				'fontSmall':'font-small',
				'fontMiddle':'font-middle',
				'fontLarge':'font-large'
			};
	if($.cookie('fontsize')!=null){
		fontChange($.cookie('fontsize'));
	}
	$fontChangeBtn.each(function(){
		var $self = $(this);
		$self.bind('click',function(){
			fontChange($self.data('font-size'));
		});
	});
	function fontChange(size){
		$root.removeClass('font-small font-middle font-large');
		$root.addClass(fontSizeObj[size]);
		$fontChangeBtn
			.removeClass('current')
			.filter('[data-font-size="'+size+'"]')
			.addClass('current');
		$.cookie('fontsize',size,{expires:30,path:'/'});
	}
});