/****
 *	Goopy Hayabusa/Websapo用 カスタマイズファンクション
 *	
 *	[ページ判定系]
 *	+ is_mobile()
 *			スマホページの場合trueを返す
 *	+ is_top()
 *			トップページの場合trueを返す
 *	+ is_content(contentName)
 *			現在のコンテンツ名と比較してboolean型で返す
 *			引数なしの場合、現在のコンテンツ名を返す
 *	+ qstr(key)
 *			パラメータの値を返す。PHPでいうところの$_GET[key]と同等
 *
 *	[ユーティリティ系]
 *	+	write_file(filePath)
 *			CSSもしくはJSファイルを読み込みませたい時に、onload外で記述。
 *			拡張子自動判断でタグ出力
 *
 ****/

// SPページ判断
function is_mobile(){
	return ($('html').hasClass('ui-mobile'))? true : false;
}
// パラメータ判断
function qstr(key){
	// URLからパラメータ配列作成
	var qstr = location.search.substring(1);
	var arr_params = qstr.split('&');
	var params =[];
	for(i=0;i<arr_params.length;i++){
		var keyval = arr_params[i].split('=');
		params[keyval[0]] = keyval[1];
	}
	// 引数のパラメータに対する値を返す
	return params[key];
}
//トップページ判断
function is_top(){
	if(location.pathname == '/' || location.pathname == '/index.php') return true;
	else return false;
}

// コンテンツ名判断
function is_content(name){
	// URLからコンテンツ名抜き出し
	var contentname = location.pathname.replace(/^\/([^\.\/]+)\/?(\.php)?(main\.php)?/g,'$1');
	// 引数の文字列と同じならtrue
	if(name!=undefined){
		return (contentname == name)? true:false;
	}else{
		return contentname;
	}
}
// ファイル読み込みファンクション
function write_file(path,type){
	//パス内の拡張子抜き出し
	if(type==undefined) var type = path.split('.').pop();
	//拡張子で書き出しタグ変更
	switch(type){
		case 'css':
			document.write(unescape('%3Clink rel="stylesheet" href="'+path+'"%3E'));
		break;
		case 'js':
			document.write(unescape('%3Cscript src="'+path+'"%3E%3C/script%3E'));
		break;
		default:
			return false;
		break;
	}
}